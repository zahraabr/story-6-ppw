from django.test import TestCase
from django.test.client import Client

# Create your tests here.
class challengeTest(TestCase):
    def test_URL_is_exist(self):
            response = Client().get('/challenge/')
            self.assertEqual(response.status_code, 200)

    def test_contains_npm(self):
            response = Client().get('/challenge/')
            response_content = response.content.decode('utf-8')
            self.assertIn("1806173613", response_content)

    def test_template_is_used(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'index.html')