from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm



# Create your views here.
def status(request):
	statusnye = Status.objects.all().values()
	if (request.method == 'POST'):
		form = StatusForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/')
	else:
		form = StatusForm()
	return render(request, 'status.html', {'statusnye': statusnye, 'form':form})