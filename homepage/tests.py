from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import status
from django.apps import apps
from .apps import HomepageConfig
from .forms import StatusForm
from .models import Status



# Create your tests here.
class Story6Test(TestCase):
    def test_URL_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status)

    def test_contains_question(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("hi! what's poppin?", response_content)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    
    def test_status_form_validation_for_blank(self):
        new_status = StatusForm(data={'status': ''})
        self.assertFalse(new_status.is_valid())
        self.assertEqual(new_status.errors['status'], ["This field is required."])
    
    def test_model_status_return_status_attribute(self):
        new_status = Status.objects.create(status='status')
        self.assertEqual(str(new_status), new_status.status)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status='status')
        count_all_new_status = Status.objects.all().count()
        self.assertEqual(count_all_new_status,1)

    def test_form_is_valid(self):
        response = self.client.post('', data={'status':'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')